<?php

return [

	'path' => public_path('assets/projects/'),
	'lores_width' => 1280,
	'hires_width' => 1920
];