var gulp = require('gulp');
var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

 // Place all Sass dependencies here
var appSass = [
    'app.scss'
];

// Place all Node JavaScript dependencies here
var nodeScripts = [
    'jquery/dist/jquery.min.js',
    'bootstrap-sass/assets/javascripts/bootstrap.min.js'
    //'picturefill/dist/picturefill.min.js'
];

// Place all app JavaScript dependencies here
var appScripts = [
    'includes/**.js',
    'app.js'
];

// Compile all the things
elixir(function(mix) {
    mix.sass(appSass, 'public/css/app.css')
        .scripts(nodeScripts, 'public/js/nodeFrameworks.js', 'node_modules')
        .scripts(appScripts, 'public/js/app.js')
        .copy('resources/assets/images/**', 'public/images/')
        .copy('node_modules/font-awesome/fonts/**', 'public/fonts/');
});