<!-- Footer -->
<footer id="contact">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 text-center">
               <h1>Interesting stuff? Need anything? <br>Contact me!</h1>
               <hr class="inv">
                <h4><strong>Kennard Vermeiren</strong></h4>
                <ul class="list-unstyled">
                    <li><i class="fa fa-phone fa-fw"></i> +32 499 29 71 22</li>
                    <li><i class="fa fa-envelope-o fa-fw"></i>  <a href="mailto:hello@kennardvermeiren.be">hello@kennardvermeiren.be</a></li>
                </ul>
                <br>
                <ul class="list-inline">
                    <li>
                        <a href="https://www.facebook.com/vermeireee" target="_blank"><i class="fa fa-facebook fa-fw fa-3x"></i></a>
                    </li>
                    <li>
                        <a href="https://be.linkedin.com/in/kennard-vermeiren-90821397" target="_blank"><i class="fa fa-linkedin fa-fw fa-3x"></i></a>
                    </li>
                </ul>
                <hr class="small inv">
                <p class="text-muted">Copyright &copy; kennard.be 2015</p>
            </div>
        </div>
    </div>
</footer>