<!-- Navigation -->
<a id="menu-toggle" href="#" class="btn btn-dark btn-lg toggle"><i class="fa fa-chevron-circle-down fa-rotate-90"></i></a>
<nav id="sidebar-wrapper">
    <ul class="sidebar-nav">
        <a id="menu-close" href="#" class="btn btn-light btn-lg pull-right toggle"><i class="fa fa-chevron-circle-down fa-rotate-270"></i></a>
        <li class="sidebar-brand">
            <a href="/home"  onclick = $("#menu-close").click(); >Kennard Vermeiren</a>
        </li>
        <li>
            <a href="/home#top" onclick = $("#menu-close").click(); >Home</a>
        </li>
        <li>
            <a href="/home#skills" onclick = $("#menu-close").click(); >Skills</a>
        </li>
        <li>
            <a href="/home#portfolio" onclick = $("#menu-close").click(); >Portfolio</a>
        </li>
        <li>
            <a href="/home#contact" onclick = $("#menu-close").click(); >Contact</a>
        </li>
    </ul>
</nav>