<!-- Navigation -->
<a id="menu-toggle" href="#" class="btn btn-dark btn-lg toggle"><i class="fa fa-chevron-circle-down fa-rotate-90"></i></a>
<nav id="sidebar-wrapper">
    <ul class="sidebar-nav">
        <a id="menu-close" href="#" class="btn btn-light btn-lg pull-right toggle"><i class="fa fa-chevron-circle-down fa-rotate-270"></i></a>
        <li class="sidebar-brand">
            <a href="{!! url('admin/dashboard') !!}"  onclick = $("#menu-close").click(); >Dashboard</a>
        </li>
        <li>
            <a href="{!! url('admin/project') !!}" onclick = $("#menu-close").click(); >Projects</a>
        </li>
        <li>
            <a href="{!! url('admin/tag') !!}" onclick = $("#menu-close").click(); >Tags</a>
        </li>
    </ul>
</nav>