<meta charset="utf-8">
<!-- Always force latest IE rendering engine (even in intranet) & Chrome Frame -->
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<title>
    @yield('title') | Kennard.be
</title>

<meta name="author" content="Kennard Vermeiren">
<meta name="description" content="This is a portfolio website." />

@yield('meta')

<meta name="csrf-token" content="{!! csrf_token() !!}" />

<!-- Mobile Specific Metas
================================================== -->
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<!-- CSS
================================================== -->
{!! Html::style('css/app.css') !!}
@yield('css')

<!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
<!--[if lt IE 9]>
<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->

<link rel="apple-touch-icon" href="apple-touch-icon.png">

@yield('js-head')