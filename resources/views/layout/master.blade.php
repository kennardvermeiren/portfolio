<!DOCTYPE html>
<html class="no-js" lang="nl">
	<head>
		@include('layout.partials.head')
	</head>

	<body class="@yield('body_class')">
		<!--[if lt IE 10]>
		    <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
		<![endif]-->
		@if(Request::is('admin/*'))
			@include('layout.partials.admin-navigation')
			@include('layout.partials.admin')
		@else
			@include('layout.partials.site-navigation')
			@include('layout.partials.site')
		@endif
		@include('layout.partials.footer')
		
		{!! Html::script('js/nodeFrameworks.js') !!}
		{!! Html::script('js/app.js') !!}
	</body>

</html>