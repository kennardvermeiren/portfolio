@extends('layout.master')

@section('js')
@endsection

@section('title', $project->title)

{{-- Add a class to the body tag for DOM-based routing in main.js --}}
@section('body_class', 'project')

{{-- Content --}}
@section('content')

<!-- Portfolio -->
<section id="project" class="topic-inv">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 text-center">
                <h1>{!! $project->title !!}</h1>
                <p>
                    @foreach ($project->tags as $tag)
                        <span class="label label-default">{!! $tag->name !!}</span>
                    @endforeach
                </p>
                <hr class="small">
                {!! Markdown::convertToHtml($project->body) !!}
            </div>
            <!-- /.col-xs-10 -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container -->
</section>

@stop