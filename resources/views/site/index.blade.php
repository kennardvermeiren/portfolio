@extends('layout.master')

@section('js')
@endsection

@section('title', 'Home')

{{-- Add a class to the body tag for DOM-based routing in main.js --}}
@section('body_class', 'home')

{{-- Content --}}
@section('content')

<!-- Header -->
<header id="top" class="header">
    <div class="text-vertical-center">
        <h1>Kennard Vermeiren</h1>
        <h2>
            I am a student based in Antwerp, Belgium and I like
        </h2> 
        <p class="lead">
            <span class="discover">software,</span>
            <span class="discover">hardware,</span>
            <span class="discover">programming,</span>
            <span class="discover">multimedia,</span>
            <span class="discover">web development,</span>
            <span class="discover">image editing,</span>
            <span class="discover">clean code,</span>
            <span class="discover">well built applications,</span>
            <span class="discover">video editing,</span>
            <span class="discover">...</span>
        </p>
        <a href="#portfolio" class="btn btn-light btn-lg"><i class="fa fa-arrow-circle-down"></i> What I do</a>
    </div>
</header>

<!-- About -->
<section id="skills" class="topic">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 text-center">
				<h2>Jack of <em>many</em> trades:</h2>
				<hr class="small inv">
				<div class="col-md-4">
					<div class="progress">
						<div class="progress-bar" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100" style="width: 70%;">
						</div>
						<span class="progress-type">html</span>
						<span class="progress-completed"></span>
					</div>
					<div class="progress">
						<div class="progress-bar progress-bar-alt" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%">
						</div>
						<span class="progress-type">css</span>
						<span class="progress-completed"></span>
					</div>
					<div class="progress">
						<div class="progress-bar" role="progressbar" aria-valuenow="55" aria-valuemin="0" aria-valuemax="100" style="width: 55%">
							<span class="sr-only"></span>
						</div>
						<span class="progress-type">js</span>
						<span class="progress-completed"></span>
					</div>
					<div class="progress">
						<div class="progress-bar  progress-bar-alt" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100" style="width: 70%">
						</div>
						<span class="progress-type">php</span>
						<span class="progress-completed"></span>
					</div>
					<div class="progress">
                        <div class="progress-bar  progress-bar" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 80%">
                        </div>
                        <span class="progress-type">laravel</span>
                        <span class="progress-completed"></span>
                    </div>
				</div>
				<div class="col-md-4">
					<div class="progress">
						<div class="progress-bar progress-bar-alt" role="progressbar" aria-valuenow="90" aria-valuemin="0" aria-valuemax="100" style="width: 90%">
						</div>
						<span class="progress-type">C#</span>
						<span class="progress-completed"></span>
					</div>
					<div class="progress">
						<div class="progress-bar" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 80%">
						</div>
						<span class="progress-type">C++</span>
						<span class="progress-completed"></span>
					</div>
					<div class="progress">
						<div class="progress-bar progress-bar-alt" role="progressbar" aria-valuenow="85" aria-valuemin="0" aria-valuemax="100" style="width: 85%">
						</div>
						<span class="progress-type">java</span>
						<span class="progress-completed"></span>
					</div>
					<div class="progress">
						<div class="progress-bar progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%">
						</div>
						<span class="progress-type">python</span>
						<span class="progress-completed"></span>
					</div>
				</div>
				<div class="col-md-4">
					<div class="progress">
						<div class="progress-bar  progress-bar" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100" style="width: 70%">
						</div>
						<span class="progress-type">photoshop</span>
						<span class="progress-completed"></span>
					</div>
					<div class="progress">
						<div class="progress-bar progress-bar-alt" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width: 75%">
						</div>
						<span class="progress-type">after effects</span>
						<span class="progress-completed"></span>
					</div>
					<div class="progress">
						<div class="progress-bar progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%">
						</div>
						<span class="progress-type">cinema 4d</span>
						<span class="progress-completed"></span>
					</div>
					<div class="progress">
						<div class="progress-bar progress-bar-alt" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width: 75%">
						</div>
						<span class="progress-type">unity 3d</span>
						<span class="progress-completed"></span>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<!-- Portfolio -->
<section id="portfolio" class="topic-inv">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 text-center">
                <h1>My projects</h1>
                <hr class="small">
                @if (count($projects) > 0)
                @foreach ($projects as $project)
                    <div class="row">
                    <h3>
                        {!! $project->title !!}
                    </h3>
                    <hr class="small">
                    <div class="col-md-12">
                        <div class="hovereffect item center-block">
                            {!! Html::image(
                                asset('assets/projects/' . $project->id . '/lowres/' . $project->imagefile . '.' . $project->mime),
                                $project->title,
                                array(  'class' => 'img-responsive'
                                    )
                                ) 
                            !!}
                            <div class="overlay">
                            	{!! Markdown::convertToHtml($project->summary) !!}
                                <p class="lead">
	                                @foreach ($project->tags as $tag)
	                                    <span class="label label-default">{!! $tag->name !!}</span>
	                                @endforeach
                                </p>
                                <a href="/projects/{!! $project->slug !!}">More info <i class="fa fa-arrow-circle-o-right"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
                @endif
            </div>
            <!-- /.col-xs-10 -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container -->
</section>

@stop