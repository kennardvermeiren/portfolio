@extends('layout.master')

{{-- Content --}}
@section('content')
@include('admin.tags.partials.navigation')

<!-- div -->
<div class="row">
	<div class="col-xs-12">
		<a class="btn btn-default btn-lg btn-block" href="{{ URL::to('admin/tag/' . $tag->id . '/edit') }}" data-toggle="tooltip" data-placement="top" title="Wijzig gegevens"><span class="glyphicon glyphicon-pencil"></span> Wijzigen</a>
		<h1><strong>{{{ trans('tag.name') }}}:</strong> {!! $tag->name!!}</h1>
	</div>
</div>
<!-- ./ div -->

@stop
