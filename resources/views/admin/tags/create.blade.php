@extends('layout.master')


{{-- content --}}
@section('content')
@include('admin.tags.partials.navigation')

<!-- div -->
<div class="row">
	<div class="col-xs-12">
		{!! Form::open(array(
			'url' => 'admin/tag',
			'novalidate' => 'novalidate',
			'files' => true
			)) !!}
		<div class="row">
			<div class="form-group @if ($errors->has('name')) has-error @endif">
				<div class="col-xs-10">
					{!! Form::label('name', trans('tag.name')) !!}
					{!! Form::text('name', Request::old('name'), array('class' => 'form-control')) !!}
					@if ($errors->has('name')) <span class="help-block">{!! $errors->first('name') !!}</span> @endif
				</div>
			</div>
		</div>

		<div class="form-group form-actions">
			{!! Form::submit('Submit', array('class' => 'btn btn-primary')) !!}
		</div>

	{!! Form::close() !!}
	</div>
</div>
<!-- ./ div -->

@stop
