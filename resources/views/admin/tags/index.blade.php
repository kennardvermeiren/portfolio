@extends('layout.master')

{{-- Content --}}
@section('content')
@include('admin.tags.partials.navigation')

<!-- div -->
<div class="row">
	<div class="col-xs-12">
		<table id="tags" class="table table-striped table-hover">
			<thead>
				<tr>
					<th class="col-xs-3">{{{ trans('tag.name') }}}</th>
					<th class="col-xs-2">{{{ trans('admin.table_actions') }}}</th>
				</tr>
			</thead>
			<tbody>
				@foreach($tags as $tag)
					<tr>
						<td><a href="{{ URL::to('admin/tag/' . $tag->id) }}">{{ $tag->name }}</a></td>
						<td>
							<div class="btn-group btn-group-sm">
								<a class="btn btn-info" href="{{ URL::to('admin/tag/' . $tag->id) }}" data-toggle="tooltip" data-placement="top" title="Toon gegevens"><i class="fa fa-info"></i></a>
								<a class="btn btn-primary" href="{{ URL::to('admin/tag/' . $tag->id . '/edit') }}" data-toggle="tooltip" data-placement="top" title="Wijzig gegevens"><i class="fa fa-pencil"></i></a>
							</div>
							<div class="pull-right">
								<!-- delete the nerd (uses the destroy method DESTROY /nerds/{id} -->
								<!-- we will add this later since its a little more complicated than the other two buttons -->
								@if (Auth::check())
									{!! Form::open([
							            'method' => 'DELETE',
							            'route' => ['admin.tag.destroy', $tag->id]]) 
							        !!}
							            {!! Form::button('<i class="fa fa-trash-o"></i>', 
							            	[	'type' => 'submit',
							            		'class' => 'btn btn-danger',
							            		'onclick' => 'return confirm("Verwijderen: Ben je zeker?")'
							            	]) 
							            !!}
							        {!! Form::close() !!}
								@endif
							</div>
						</td>
					</tr>
					@endforeach
			</tbody>
		</table>
	</div>
</div>
<!-- ./ div -->

@stop
