@extends('layout.master')

{{-- Content --}}
@section('content')
@include('admin.projects.partials.navigation')

<!-- div -->
<div class="row">
	<div class="col-xs-12">
		{!! Form::model($project, array(
			'route' => ['admin.project.update', $project->id],
			'method' => 'PATCH', 
			'files' => true
			)) !!}
		<div class="row">
			<div class="form-group @if ($errors->has('title')) has-error @endif">
				<div class="col-xs-10">
					{!! Form::label('title', trans('project.title')) !!}
					{!! Form::text('title', null, array('class' => 'form-control')) !!}
					@if ($errors->has('title')) <span class="help-block">{!! $errors->first('title') !!}</span> @endif
				</div>
			</div>

			<div class="form-group @if ($errors->has('active')) has-error @endif">
				<div class="col-xs-2">
					{!! Form::label('active', trans('project.active')) !!}
					<input type="checkbox" name="active" value="1" id="active" class="form-control" {!! $project->active ? "checked" : "" !!}>
					@if ($errors->has('active')) <span class="help-block">{!! $errors->first('active') !!}</span> @endif
				</div>
			</div>
		</div>

		<div class="row">
			<div class="form-group @if ($errors->has('tags')) has-error @endif">
				<div class="col-xs-12">
					{!! Form::label('tags', trans('project.tags')) !!}
					{!! Form::select('tags[]', $tags, $project->tags->lists('id')->all(), ['multiple' => 'multiple', 'class' => 'form-control', 'id' => 'tags']) !!}
					@if ($errors->has('tags')) <span class="help-block">{!! $errors->first('tags') !!}</span> @endif
				</div>
			</div>
		</div>

		<div class="row">
			<div class="form-group @if ($errors->has('summary')) has-error @endif">
				<div class="col-xs-12">
					{!! Form::label('summary', trans('project.summary')) !!}
					{!! Form::textarea('summary', Request::old('summary'), array('class' => 'summernote form-control', 'id' => 'summary')) !!}
					@if ($errors->has('summary')) <span class="help-block">{!! $errors->first('summary') !!}</span> @endif
				</div>
			</div>
		</div>

		<div class="row">
			<div class="form-group @if ($errors->has('body')) has-error @endif">
				<div class="col-xs-12">
					{!! Form::label('body', trans('project.body')) !!}
					{!! Form::textarea('body', null, array('class' => 'summernote form-control', 'id' => 'body')) !!}
					@if ($errors->has('body')) <span class="help-block">{!! $errors->first('body') !!}</span> @endif
				</div>
			</div>
		</div>

		<div class="row">
		<div class="form-group @if ($errors->has('imagefile')) has-error @endif">
			<div class="col-xs-12">
				{!! Form::label('imagefile', trans('project.imagefile')) !!}
				{!! Form::file('imagefile', array('class' => 'form-control', 'id' => 'imagefile')) !!}
				@if ($errors->has('imagefile')) <span class="help-block">{!! $errors->first('imagefile') !!}</span> @endif
			</div>
		</div>
		</div>

		<div class="form-group form-actions">
			{!! Form::submit('Submit', array('class' => 'btn btn-primary')) !!}
		</div>

	{!! Form::close() !!}
	</div>
</div>
<!-- ./ div -->

@stop
