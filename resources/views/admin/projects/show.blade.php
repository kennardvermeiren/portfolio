@extends('layout.master')

{{-- Content --}}
@section('content')
@include('admin.projects.partials.navigation')

<!-- div -->
<div class="row">
	<div class="col-xs-12">
		<a class="btn btn-default btn-lg btn-block" href="{{ URL::to('admin/projects/' . $project->id . '/edit') }}" data-toggle="tooltip" data-placement="top" title="Wijzig gegevens"><span class="glyphicon glyphicon-pencil"></span> Wijzigen</a>
		<h1><strong>{{{ trans('project.title') }}}:</strong> {!! $project->title !!}</h1>
		<h2><strong>{{{ trans('project.summary') }}}:</strong></h2>
		<p>
			{!! $project->summary !!}
		</p>
		<h2><strong>{{{ trans('project.body') }}}:</strong></h2>
		<p>
			{!! $project->body !!}
		</p>
	</div>
</div>
<!-- ./ div -->

@stop
