<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;
use League\CommonMark\Converter;

class Project extends Model
{

	protected $table = 'projects';
    protected $fillable = ['title', 'summary', 'body', 'filename', 'active'];
    public $timestamps = true;

    protected $converter;

    /*public function __construct(Converter $converter)
    {
        $this->converter = $converter;
    }*/

    public function tags() {
    	return $this->belongsToMany('App\Tag');
    }

    public function setTitleAttribute($value)
	{
		$this->attributes['title'] = $value;

		if (! $this->exists) {
			$this->attributes['slug'] = str_slug($value);
		}
	}
}
