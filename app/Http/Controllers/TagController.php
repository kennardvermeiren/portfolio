<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Tag;
use App\Http\Requests\CreateTagRequest;
use App\Http\Requests\UpdateTagRequest;

class TagController extends Controller
{
    protected $tag;

	public function __construct(Tag $tag) {
		$this->tag = $tag;
	}

    public function index() {
    	$tags = Tag::all();
    	return view('admin.tags.index', compact('tags'));
    }

    public function create() {
    	return view('admin.tags.create');
    }

    public function store(CreateTagRequest $request) {
    	$tag = new Tag;
    	$tag->fill($request->all());
        $tag->save();

    	return redirect('admin/tag')->with('success', 'Tag created');
    }

    public function show($id) {
    	$tag = Tag::findOrFail($id);
    	return view('admin.tags.show', compact('tag'));
    }

    public function edit($id) {
    	$tag = Tag::findOrFail($id);
    	return view('admin.tags.edit', compact('tag'));
    }

    public function update($id, UpdateTagRequest $request) {
    	$tag = Tag::findOrFail($id);
    	$tag->fill($request->all());
        $tag->save();

    	return redirect('admin/tag')->with('success', 'Tag edited');
    }

    public function destroy($id) {
    	$tag = Tag::findOrFail($id);
    	$tag->delete();
    	return redirect('admin/tag')->with('success', 'Tag deleted');
    }
}
