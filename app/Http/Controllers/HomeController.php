<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

use App\Project;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $projects = Project::where('active', 1)
            ->orderBy('updated_at', 'desc')
            ->get();

        $storagePath  = Storage::disk('local')->getDriver()->getAdapter()->getPathPrefix();
        return view('site.index', compact('projects', 'storagePath'));
    }

    public function showProject($slug) {
        $project = Project::whereSlug($slug)->firstOrFail();
        return view('site.project')->withProject($project);
    }
}
