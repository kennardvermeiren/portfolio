<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

use App\Project;
use App\Tag;

use File;

use App\Http\Requests\CreateProjectRequest;
use App\Http\Requests\UpdateProjectRequest;

class ProjectController extends Controller
{
	protected $project;

	public function __construct(Project $project) {
		$this->project = $project;
	}

    public function index() {
    	$projects = Project::orderBy('updated_at', 'desc')->get();
    	return view('admin.projects.index', compact('projects'));
    }

    public function create() {
        $tags = Tag::lists('name', 'id');
    	return view('admin.projects.create', compact('tags'));
    }

    public function store(CreateProjectRequest $request) {
    	$project = new Project;
        $project->fill($request->except('tags'));
        $project->save();
        $project->tags()->attach($request->tags);

        $file = $request->file('imagefile');
        $name = uniqid();
        $extension = $file->getClientOriginalExtension();

        $pathHiRes = config('projects.path') . $project->id . '/hires';
        $pathLowRes = config('projects.path') . $project->id . '/lowres';

        if (!file_exists($pathHiRes)) {
            File::makeDirectory($pathHiRes, $mode = 0777, true);
        }
        if (!file_exists($pathLowRes)) {
            File::makeDirectory($pathLowRes, $mode = 0777, true);
        }

        $img = \Img::make($file->getRealPath());
        $imgLowRes = \Img::make($file->getRealPath());
        $imgLowRes->resize(config('projects.lores_width'), null, function($constraint) {
            $constraint->aspectRatio();
            $constraint->upsize();
        });
        $img->save($pathHiRes . '/' . $name . '.' . $extension);
        $imgLowRes->save($pathLowRes . '/' . $name . '.' . $extension);

        $project->imagefile = $name;
        $project->mime = $extension;
        $project->save();

        return redirect('admin/project')->with('success', 'Project created');
    }

    public function show($id) {
    	$project = Project::findOrFail($id);
    	return view('admin.projects.show', compact('project'));
    }

    public function edit($id) {
    	$project = Project::findOrFail($id);
        $tags = Tag::lists('name', 'id');
    	return view('admin.projects.edit', compact('project', 'tags'));
    }

    public function update($id, UpdateProjectRequest $request) {
        $project = Project::findOrFail($id);
        $project->fill($request->except('imagefile'));
        if ($request->tags) {
            $project->tags()->sync($request->tags);
        }
        
        if ($request->file('imagefile')) {
            $file = $request->file('imagefile');
            $extension = $file->getClientOriginalExtension();

            $name = uniqid();

            $pathHiRes = config('projects.path') . $project->id . '/hires';
            $pathLowRes = config('projects.path') . $project->id . '/lowres';

            if (!file_exists($pathHiRes)) {
                File::makeDirectory($pathHiRes, $mode = 0777, true);
            }
            if (!file_exists($pathLowRes)) {
                File::makeDirectory($pathLowRes, $mode = 0777, true);
            }

            $img = \Img::make($file->getRealPath());
            $imgLowRes = \Img::make($file->getRealPath());
            $imgLowRes->resize(config('projects.lores_width'), null, function($constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
            });
            $img->save($pathHiRes . '/' . $name . '.' . $extension);
            $imgLowRes->save($pathLowRes . '/' . $name . '.' . $extension);

            // DELETING OLD FILE
            $fileToDeleteHi = $pathHiRes . '/' . $project->imagefile . '.' . $project->mime;
            $fileToDeleteLo = $pathLowRes . '/' . $project->imagefile . '.' . $project->mime;
            if (file_exists($fileToDeleteHi)) {
                File::delete($fileToDeleteHi);
            }
            if (file_exists($fileToDeleteLo)) {
                File::delete($fileToDeleteLo);
            }

            $project->imagefile = $name;
            $project->mime = $extension;
        }

        $project->save();

        return redirect('admin/project')->with('success', 'Project edited');
    }

    public function destroy($id) {
    	$project = Project::findOrFail($id);
        $project->tags()->detach();

        $folder = config('projects.path') . $project->id;
        if (file_exists($folder)) {
            File::deleteDirectory($folder);
        }
        $project->delete();
        return redirect('admin/project')->with('success', 'Project deleted');
    }

    public function activate($id) {
        $project = Project::findOrFail($id);
        $project->active = 1;
        $project->timestamps = false;
        $project->save();
        $project->timestamps = true;
        return redirect('admin/project')->with('success', 'Project activated!');
    }
    public function deactivate($id) {
        $project = Project::findOrFail($id);
        $project->active = 0;
        $project->timestamps = false;
        $project->save();
        $project->timestamps = true;
        return redirect('admin/project')->with('success', 'Project deactivated!');
    }
}
